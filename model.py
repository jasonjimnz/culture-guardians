import sqlalchemy, datetime
import os, binascii
import json
from sqlalchemy import create_engine
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy import Boolean, DateTime, Text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker

from werkzeug.security import generate_password_hash, \
    check_password_hash

from settings import Settings as s

# DATA Models
Base = declarative_base()

def generate_random_token(value=32):
    return binascii.b2a_hex(os.urandom(value))

def get_password_hash(password):
    return generate_password_hash(password)

def check_password(password_hash, password):
    return check_password_hash(password_hash, password)


class User(Base):
    __tablename__ = "user"

    id = Column(Integer, primary_key=True)
    email = Column(String(200), nullable=False, unique=True)
    first_name = Column(String(200), nullable=True)
    last_name = Column(String(200), nullable=True)
    password = Column(String(1024), nullable=False)
    user_hash = Column(String(2048), nullable=True)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    deleted_at = Column(DateTime, nullable=True)
    active = Column(Boolean, default=False)
    activation_token = Column(String(512), nullable=True, unique=True)
    is_admin = Column(Boolean, default=False)
    is_superadmin = Column(Boolean, default=False)

class UserSession(Base):
    __tablename__ = "user_session"

    id = Column(Integer, primary_key=True)
    session_token = Column(String(2048), nullable=False)
    user = relationship(User)
    user_id = Column(Integer, ForeignKey('user.id'), nullable=True)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    deleted_at = Column(DateTime, nullable=True)
    active = Column(Boolean, default=True)

class Page(Base):
    __tablename__ = "page"

    id = Column(Integer, primary_key=True)
    title = Column(String(256), nullable=False)
    slug = Column(String(256), nullable=False, unique=True)
    content = Column(Text, nullable=False)
    seo_tags = Column(String(512))
    seo_description = Column(String(1024))
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    deleted_at = Column(DateTime, nullable=True)
    active = Column(Boolean, default=True)

class City(Base):
    __tablename__ = "city"

    id = Column(Integer, primary_key=True)
    name = Column(String(256), nullable=False)
    slug = Column(String(256), nullable=False, unique=True)
    minube_id = Column(Integer)
    latitude = Column(Integer)
    longitude = Column(Integer)
    seo_tags = Column(String(512))
    seo_description = Column(String(1024))
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    deleted_at = Column(DateTime, nullable=True)
    active = Column(Boolean, default=True)


class Model():
    engine = None
    session = None
    tables = [User, City]

    def __init__(self, engineType = 'sqlite', debug = False):
        self.engine = self.start_engine(engineType, debug)
        self.init_database()
        self.check_data_integrity()

    def start_engine(self, engineType, debug = False):
        if engineType:
            self.engine = create_engine(
                self.get_engine(engineType),
                echo = debug
                )
        return self.engine

    def init_database(self):
        Base.metadata.create_all(self.engine)
        Base.metadata.bind = self.engine
        DBSession = sessionmaker(bind=self.engine)
        self.session = DBSession()
        return True

    def get_engine(self, engineType):
        if engineType == "sqlite":
            return 'sqlite:///database.db'
        elif engineType == "mysql":
            return 'mysql://%s:%s@%s/%s' % ( 
                s['DATABASE_USER'], 
                s['DATABASE_PASS'], 
                s['DATABASE_HOST'], 
                s['DATABASE_NAME']
                )
        else:
            return None

    def delete_entity(self, entity):
        self.session.delete(entity)
        self.session.commit()
        self.session.close()
        return True

    def deactivate_entity(self, entity):
        entity.updated_at = datetime.datetime.now()
        entity.deleted_at = datetime.datetime.now()
        entity.active = False
        self.session.add(entity)
        self.session.commit()
        self.session.close()
        return True

    def get_user_by_email(self, email):
        u = self.session.query(User).filter(User.email == email)
        try:
            return u.one()
        except Exception as e:
            return None

    def check_data_integrity(self):
        l = len(self.session.query(User).all())
        if l == 0:
            self.load_users_from_fixtures()
        l = len(self.session.query(City).all())
        if l == 0:
            self.load_cities_from_fixtures()
        return True

    def load_users_from_fixtures(self):
        o = open('fixtures.json', 'r')
        j = json.load(o)
        users = j[0]['users']
        for u in users:
            hash_user = get_password_hash(u['pass'])
            print hash_user
            user = User(
                email = u['user'],
                password = hash_user,
                created_at = datetime.datetime.now(),
                updated_at = datetime.datetime.now(),
                activation_token = generate_random_token(),
                is_admin = u['admin'],
                is_superadmin = u['superadmin'],
                active = True
                )
            self.session.add(user)
            self.session.commit()
            self.session.close()
        return True

    def load_cities_from_fixtures(self):
        o = open('fixtures.json', 'r')
        j = json.load(o)
        cities = j[0]['cities']
        for c in cities:
            city = City(
                name = c['city'],
                slug = c['city'],
                minube_id = c['minube_id'],
                created_at = datetime.datetime.now(),
                updated_at = datetime.datetime.now()
                )
            self.session.add(city)
            self.session.commit()
            self.session.close()
        return True

    def get_user_by_id(self, user_id):
        try:
            u = self.session.query(User).filter(User.id == int(user_id))
            return u.one()
        except Exception as e:
            return None

    def get_users(self, email):
        u = self.session.query(User).filter(User.email.like('%'+email+'%')).all()
        return u

    def activate_user(self, activation_token):
        u = self.session.query(User).filter(User.activation_token == activation_token)
        try:
            u = u.one()
            u.active = True
            u.activation_token = None
            self.session.add(u)
            self.session.commit()
            self.session.close()
            return True
        except Exception as e:
            return False

    def create_user(self, email, password ):
        hash_user = get_password_hash(password)
        user = User(
            email = email,
            password = hash_user,
            created_at = datetime.datetime.now(),
            updated_at = datetime.datetime.now(),
            activation_token = generate_random_token()
            )
        self.session.add(user)
        self.session.commit()
        self.session.close()
        return True

    def update_user(self, user, **fields):
        for x in fields:
            try:
                if fields[x] != 'password':
                    setattr(user, x, fields[x])
                else:
                    setattr(user, x, get_password_hash(fields[x]))
            except Exception as e:
                pass
        user.updated_at = datetime.datetime.now()
        self.session.add(user)
        self.session.commit()
        self.session.close()
        return user
 
    def create_user_session(self, user):
        user_session = UserSession(
                session_token = generate_random_token(1024),
                user = user,
                created_at = datetime.datetime.now(),
                updated_at = datetime.datetime.now()
            )
        token = user_session.session_token
        self.session.add(user_session)
        self.session.commit()
        self.session.close()
        return token

    def get_user_session_by_token(self, token):
        try:
            s = self.session.query(UserSession).filter(UserSession.session_token == token , UserSession.active == True).one()
        except Exception as e:
            s = None

        return s

    def get_user_sessions_by_user(self, email):
        u = self.get_user_by_email(email)
        s = self.session.query(UserSession).filter(UserSession.user == u , UserSession.active == True)
        return s.all()

    def get_cities(self):
        c = self.session.query(City).filter(City.active == True).all()
        return c

    def get_city_by_name(self, name):
        try:
            c = self.session.query(City).filter(City.name == name, City.active == True).one()
        except Exception as e:
            c = None
        return c

    def get_city_by_id(self, city_id):
        try:
            c = self.session.query(City).filter(City.id == city_id, City.active == True).one()
        except Exception as e:
            c = None
        return c

    def search_cities_by_name(self, query):
        c = self.session.query(City).filter(City.name.like('%'+query+'%'))
        return c.all()

    def create_city(self, name, minube_id = None):
        new_city = City(
            name = name,
            slug = name,
            minube_id = minube_id,
            created_at = datetime.datetime.now(),
            updated_at = datetime.datetime.now()
            )
        self.session.add(new_city)
        self.session.commit()
        self.session.close()
        return True

    def update_city(self, city, **fields):
        for x in fields:
            try:
                setattr(city, x, fields[x])
            except Exception as e:
                pass
        city.updated_at = datetime.datetime.now()
        self.session.add(city)
        self.session.commit()
        self.session.close()
        return True

    def get_pages(self):
        p = self.session.query(Page).filter(Page.active == True)
        return p.all()

    def get_pages_by_title(self, title):
        p = self.session.query(Page).filter(Page.title.like('%'+title+'%'))
        return p.all()

    def get_page_by_id(self, page_id):
        try:
            p = self.session.query(Page).filter(Page.id == page_id).one()
        except Exception, e:
            p = None
        return p

    def get_page_by_slug(self, page_slug):
        try:
            p = self.session.query(Page).filter(Page.slug == page_slug).one()
        except Exception, e:
            p = None
        return p

    def create_page(self, title, slug, content, seo_description = None, seo_tags = None):
        new_page = Page(
            title = title,
            slug = slug,
            content = content,
            seo_description = seo_description,
            seo_tags = seo_tags,
            created_at = datetime.datetime.now(),
            updated_at = datetime.datetime.now()
            )
        self.session.add(new_page)
        self.session.commit()
        self.session.close()
        return slug

    def update_page(self, page, **fields):
        for x in fields:
            try:
                setattr(page, x, fields[x])
            except Exception as e:
                pass
        page.updated_at = datetime.datetime.now()
        self.session.add(page)
        self.session.commit()
        self.session.close()
        return True

