from flask import Flask, request, session, g, redirect, url_for, abort, \
	render_template, escape
from flask_mail import Mail, Message

#Model imports
from model import Model, generate_random_token ,get_password_hash, check_password

#Settings
from settings import Settings as S

#APIs
from APIs.miNube import getPois
from APIs.luis import respuestaLUIS, respuesta
from APIs.Translate import translateToSpanish
from APIs.spellCheck import corregirTexto

app = Flask(__name__)

app.config.from_object(__name__)
app.config['MAIL_SERVER']=S['MAIL_SERVER']
app.config['MAIL_PORT'] = S['MAIL_PORT']
app.config['MAIL_USERNAME'] = S['MAIL_USERNAME']
app.config['MAIL_PASSWORD'] = S['MAIL_PASSWORD']
#app.config['MAIL_USE_TLS'] = False
#app.config['MAIL_USE_SSL'] = True

mail = Mail(app)

#MOUDLES
from modules.geospatialJson import createGeoSpatialJson

#MODEL
try:
	m = Model(S['DATABASE_ENGINE'])
except Exception as e:
	import traceback; print traceback.format_exc()
	m = Model()

def get_user_by_cookie():
	response = None
	if 'session_token' in session:
		session_token = m.get_user_session_by_token(session['session_token'])
		if session_token:
			response = session_token.user
	return response

def get_default_context():
	context = {
		'msj' : 'Culture Guardians',
		'user': get_user_by_cookie()
	}
	return context

def send_test_mail():
	try:
		msg = Message("Hello",
					  sender="info@jasonjimnzdev.es",
					  recipients=["jasonjimenezcruz@gmail.com"])
		msg.body = "This is the email body"
		mail.send(msg)
	except Exception, e:
		print("No se pudo enviar el email")
	return True

def sendRegisterEmail(to, token):
	try:
		msg = Message("Confirma tu registro en Culture Guardians",
			sender ="info@jasonjimnzdev.es",
			recipients=[to])
		link = '/activate_user/%s' % token
		msg.body = "<p>Tu cuenta ha sido creada pero no activada, para hacerlo visita el siguiente enlace: %s </p>" % link
	except Exception, e:
		raise e
#CONTROLLERS: Add controllers

@app.route("/")
def homeController():
	#TODO: Fill context
	context = get_default_context()

	return render_template('index.html', context=context)

@app.route('/api_test')
def apiTest():
	#getPois('as','as')
	#respuestaLUIS('')
	#translateToSpanish('')
	corregirTexto('')

	context = get_default_context()

	return render_template('index.html', context=context)

@app.route('/login', methods=['GET','POST'])
def login():
	if request.method == 'GET' and 'username' not in session:
		context = get_default_context()
		return render_template('login.html', context=context)
	elif request.method == 'POST' and 'username' not in session:
		u = request.form['email']
		p = request.form['auth']
		user = m.get_user_by_email(u)
		if user and user.active:
			response = check_password(user.password, p)
			if response:
				user_session = m.create_user_session(user)
				session['username'] = u
				session['session_token'] = user_session
			else:
				pass
				#TODO: Manage login errors
		elif user and not user.active:
			pass
			#TODO: Manage Non-activated users
		else:
			pass
			#TODO: Manage non-existing user login attempt
	return redirect('/')

@app.route('/logout', methods=['GET'])
def logout():
	if 'session_token' in session:
		user_session = m.get_user_session_by_token(session['session_token'])
	else:
		user_session = None
	if user_session:
		m.deactivate_entity(user_session)
	session.pop('username', None)
	session.pop('session_token', None)
	return redirect(url_for('login'))

@app.route('/register', methods=['GET','POST'])
def register():
	if get_user_by_cookie():
		return redirect(url_for('homeController'))
	if request.method == 'GET':
		context = get_default_context()
		return render_template('register.html', context=context)
	elif request.method == 'POST':
		u = request.form['email']
		p = request.form['auth']
		user = m.create_user(u,p)
		#TODO: Manage register errors
	return redirect(url_for('login'))

@app.route('/activate_user/<activation_hash>', methods=['GET'])
def activateUser(activation_hash):
	if get_user_by_cookie():
		return redirect(url_for('homeController'))
	activation = m.activate_user(activation_hash)
	if activation:
		return redirect(url_for('login'))
	else:
		return redirect(url_for('homeController'))

@app.route('/search_trips', methods=['POST'])
def seachTrips():
	#getPois('as','as')
	#respuestaLUIS('')
	#translateToSpanish('')
	texto = request.form['searchQuery']
	r = respuesta(texto)
	l = m.search_cities_by_name(r.lugar)
	context = get_default_context()
	context['respuesta'] = r
	if len(l) == 0:
		context['patrimonio'] = False
	else:
		context['patrimonio'] = True
		
	if r:
		context['matching'] = getPois('es', r.lugar)
	else:
		context['matching'] = None
	print context['matching'] 
	print r

	return render_template('search_results.html', context=context)

@app.route('/about', methods=['GET'])
def aboutUs():
	context = get_default_context()
	return render_template('about_us.html', context=context)

#ASYNC HTML CONTROLLERS

@app.route('/api/html/get_carto_map/', methods=['GET'])
def apiHtmlGetCartoMap():
	context = get_default_context()
	context['async'] = True
	return render_template('carto_map.html', context=context)

#ASYNC JSON CONTROLLERS

@app.route('/api/json/get_geospatial_json', methods=['POST'])
def apiJsonGetGeospatialJson():
	name = request.form['name']
	amenity = request.form['amenity']
	latitude = float(request.form['latitude'])
	longitude = float(request.form['longitude'])
	try:
		popupContent = request.form['popupContent']
	except Exception, e:
		popupContent = None
	geo = createGeoSpatialJson(name, amenity, latitude, longitude, popupContent)
	return geo

#POST CONTROLLERS:

app.secret_key = S['SECRET_KEY']

if __name__ == "__main__":
	app.run('0.0.0.0', debug=True)