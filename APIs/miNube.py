# -*- coding: utf-8 -*-
import requests
# from model import Model
import json

apiKey = "teyg6PT9YCy6erSc"

def getPois(lang, city):
	try:
		m = Model()
	except:
		return None
	
	city = m.get_city_by_name(city.lower())
	city_id = city.minube_id
	
	if city_id == None:
		return None
    	
	#city_id = "4907" # Borrar
	#lang = "es" # Borrar
	url = "http://papi.minube.com/pois?lang=" + lang + "&city_id=" + city_id + "&category_group=tosee&order_by=score&api_key=" + apiKey

	r = requests.get(url)

	if not r.ok: # Returns status code
		return None

	jsonListPois = json.loads(r.content)
	
	if jsonListPois == None or len(jsonListPois) == 0:
		return None
	
	listPois = []
	for jsonPoi in jsonListPois:
		poi = Poi(jsonPoi['name'], jsonPoi['latitude'], jsonPoi['longitude'], jsonPoi['picture_url'])
		listPois.append(poi)

	return listPois

class Poi():
	name = None
	latitud = None
	longitud = None
	urlImagen = None
	def __init__(self, name, latitud, longitud, urlImagen):
		self.name = name
		self.latitud = latitud
		self.longitud = longitud
		self.urlImagen = urlImagen