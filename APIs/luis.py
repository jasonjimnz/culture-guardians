# -*- coding: utf-8 -*-
import requests
import json

from APIs.Translate import translateToSpanish
from APIs.spellCheck import corregirTexto

apiKey = "5c8775e6d68d4b6ea40a9c4666635cf1"

def respuesta(query, entidad = None):
	if query == None or query == "":
		return None 

	if entidad == None:
		entidad = respuestaLUIS(query)
	else:
		entidad = respuestaFaltaEntidad(query, entidad)

	return entidad

def respuestaLUIS(query):
	query = corregirTexto(query)
	query = translateToSpanish(query)

	url = "https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/04a8ab3a-6d86-40ab-aeab-1ab8075306dd?subscription-key=" + apiKey + "&q=" + query

	r = requests.get(url)

	if not r.ok: # Returns status code
		return None

	respuestaLuis = json.loads(r.content)
	
	if respuestaLuis == None:
		return None
	
	intent = respuestaLuis["topScoringIntent"]["intent"]
	if intent != "viajar":
		return None

	listEntidades = respuestaLuis["entities"]
	entidades = Entidades()

	for entidad in listEntidades:
		tipo = entidad["type"]
		nombreEntidad = entidad["entity"]
		if tipo == "lugar":
			entidades.lugar = nombreEntidad
		elif tipo == "builtin.number":
			entidades.numero = nombreEntidad
		elif tipo == "duracion":
			entidades.duracion = nombreEntidad

	entidades.mensaje = faltaEntidad(entidades)

	return entidades

def respuestaFaltaEntidad(query, entidad):
	if entidad.lugar == None:
		entidad.lugar = query
	else:
		entidad2 = respuestaLUIS(query)
		entidad.numero = entidad2.numero
		entidad.duracion = entidad2.duracion

class Entidades():
	mensaje = None
	lugar = None
	duracion = None
	numero = None
	def __init__(self, lugar = None, duracion = None, numero = None, mensaje = None):
		self.lugar = lugar
		self.duracion = duracion
		self.numero = numero
		self.mensaje = mensaje

def faltaEntidad(entidad):
	if entidad.lugar == None:
		return "¿A donde quieres ir?"
	
	if entidad.duracion == None or entidad.numero == None:
		return "¿Cuanto tiempo tienes planeado estar allí?"

	return "OK"